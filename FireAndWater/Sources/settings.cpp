#include "Headers/settings.h"

Settings::Settings()
{
    _melody = false;
}

void Settings::setMelody(int isMelodyOn)
{
    if(!isMelodyOn)
        _melody = false;
    else
        _melody = true;
}

bool Settings::getMelody()
{
    return _melody;
}

Settings::~Settings(){}

